package com.aat.application.views;

import com.aat.application.data.entity.ZJTProduct;
import com.aat.application.data.service.DemoService;
import com.aat.application.data.service.ProductService;
import com.aat.application.form.ProductForm;
import com.aat.application.form.ProductForm.DeleteEvent;
import com.aat.application.form.ProductForm.SaveEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Trip Component")
@Route(value = "tripcomponent", layout = MainLayout.class )

public class TripComponentView  extends VerticalLayout{

	private Grid<ZJTProduct> grid = new Grid<>(ZJTProduct.class);
	TextField filterText = new TextField();
	
	private ProductForm form;
	
	private ProductService service;
	
	public TripComponentView(ProductService service) {
		this.service = service;
		
		
		setSizeFull();
		
		configureGrid();
		configureForm();
		getContent();
		add(getToolbar(), getContent());
		updateList();
		closeEditor();
	}

	private Component getContent() {
		HorizontalLayout content = new HorizontalLayout(grid, form);
		content.setFlexGrow(2,  grid);
		content.setFlexGrow(1, form);
        content.addClassNames("content");
        content.setSizeFull();
        return content;
	}

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY); 
        filterText.addValueChangeListener(e -> updateList());

        Button addContactButton = new Button("Add product");
        addContactButton.addClickListener(click -> add());
        
        Button populateButton = new Button("Populate Components");
        populateButton.addClickListener(click -> populateTripComponents());

        var toolbar = new HorizontalLayout(filterText, addContactButton, populateButton); 

        toolbar.addClassName("toolbar"); 

        return toolbar;
    }

	private void configureGrid() {
		grid.addClassName("scheduler-grid");
		
		grid.setSizeFull();
		grid.setColumns("name", "description");
		grid.addColumn(product -> product.getResourceType().getName()).setHeader("Resource Type");
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
		grid.asSingleSelect().addValueChangeListener(event -> edit(event.getValue()));
	}
	
	private void configureForm()
	{
		form = new ProductForm(service.getResourceTypes(), service.getTripElements());
		form.setWidth("25em");

		form.addSaveListener(this::save);
		form.addDeleteListener(this::delete);
		form.addCloseListener(e -> closeEditor());
	}

	private void updateList() {
		grid.setItems(service.findAllTripComponent(filterText.getValue()));
		
	}
	
	public void edit(ZJTProduct product)
	{
		if (product == null) {
			closeEditor();
		} else {
			form.setProduct(product);
			form.setVisible(true);
			addClassName("editing");
		}
	}
	
	private void add()
	{
		grid.asSingleSelect().clear();
		edit(new ZJTProduct());
	}

	private void populateTripComponents()
	{
		service.populateComponents();
		updateList();
	}

	private void closeEditor() {
		form.setProduct(null);
		form.setVisible(false);
		removeClassName("editing");
		
	}

	private void save(ProductForm.SaveEvent event)
	{
		service.save(event.getProduct());
		updateList();
		closeEditor();
	}
	
	private void delete(ProductForm.DeleteEvent event)
	{
		service.delete(event.getProduct());
		updateList();
		closeEditor();
	}
}
