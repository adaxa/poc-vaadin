package com.aat.application.views;

import com.aat.application.data.entity.ZJTElement;
import com.aat.application.data.service.PricingTypeService;
import com.aat.application.data.service.TripElementService;
import com.aat.application.form.PricingTypeForm;
import com.aat.application.form.TripElementForm;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Trip Element")
@Route(value="tripelement", layout = MainLayout.class )

public class TripElementView  extends VerticalLayout{

	private Grid<ZJTElement> grid = new Grid<>(ZJTElement.class);
	TextField filterText = new TextField();
	
	private TripElementForm form;
	
	private TripElementService service;
	
	public TripElementView(TripElementService service) {
		this.service = service;
		
		
		setSizeFull();
		
		configureGrid();
		configureForm();
		getContent();
		add(getToolbar(), getContent());
		updateList();
		closeEditor();
	}

	
	private Component getContent() {
		HorizontalLayout content = new HorizontalLayout(grid, form);
		content.setFlexGrow(2,  grid);
		content.setFlexGrow(1, form);
        content.addClassNames("content");
        content.setSizeFull();
        return content;
	}

    private HorizontalLayout getToolbar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addButton = new Button("Add");
        addButton.addClickListener(click -> add());
        var toolbar = new HorizontalLayout(filterText, addButton); 

        toolbar.addClassName("toolbar"); 

        return toolbar;
    }

	private void configureGrid() {
		grid.addClassName("scheduler-grid");
		
		grid.setSizeFull();
//		grid.setColumns("name", "zjt_pricingtype_id");
		grid.setColumns("name", "uom", "elementlist");
		grid.addColumn(pricingtype -> pricingtype.getPricingType().getName()).setHeader("Pricing Type");
//		grid.addColumn(product -> product.getName()).setHeader("Product Name");
//		grid.getColumnByKey("zjt_pricingtype_id").setHeader("Pricing Type");
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
		grid.asSingleSelect().addValueChangeListener(event -> edit(event.getValue()));
	}
	
	private void configureForm()
	{
		form = new TripElementForm(service.getPricingTypes());
		form.setWidth("25em");

		form.addSaveListener(this::save);
		form.addDeleteListener(this::delete);
		form.addCloseListener(e -> closeEditor());
	}

	private void updateList() {
		grid.setItems(service.findAllElements(filterText.getValue()));
		
	}
	
	public void edit(ZJTElement po)
	{
		if (po == null) {
			closeEditor();
		} else {
			form.setBean(po);
			form.setVisible(true);
			addClassName("editing");
		}
	}
	
	private void add()
	{
		grid.asSingleSelect().clear();
		edit(new ZJTElement());
	}



	private void closeEditor() {
		form.setBean(null);
		form.setVisible(false);
		removeClassName("editing");
		
	}

	private void save(TripElementForm.SaveEvent event)
	{
		service.save(event.getBean());
		updateList();
		closeEditor();
	}
	
	private void delete(TripElementForm.DeleteEvent event)
	{
		service.delete(event.getBean());
		updateList();
		closeEditor();
	}
}
